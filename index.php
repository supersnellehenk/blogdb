<?php
include 'PHP/scripts/connection.php';
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="css/styles.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
</head>
<body onload="getPosts()">
	<div class="btn-group" id="navmenu">
		<?php if (isset($_SESSION['user'])) {
    ?>
			<a data-toggle="modal" href="#uitloggen" class="btn btn-primary">Uitloggen</a>
			<a data-toggle="modal" href="#berichtMaken" class="btn btn-primary">Bericht Aanmaken</a>
			<?php
} else {
        ?>
			<a data-toggle="modal" href="#loginRegist" class="btn btn-primary">Login/Registreren</a>
			<?php
    } ?>
	</div>
	<div class="container">
		<?php if (isset($_SESSION['user'])) {
        echo 'Welkom ' . ucfirst($_SESSION['user']); ?>
			<br><img src='<?= $_SESSION['image'] ?>' height="80px">
			<?php
		}
        ?>
		<div id="msgContainer" class="row my-2">
		</div>
	</div>
	<div id="loginRegist" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Inloggen/Registreren</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<ul class="nav nav-tabs" id="tabContent">
					<li><a class="active" id="loginA" href="#login" data-toggle="tab">Login</a></li>
					<li><a id="registerA" href="#register" data-toggle="tab">Register</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="login">
						<form action="PHP/scripts/authentication.php" method="post" id="loginForm"
						enctype="multipart/form-data">
						<div class="form-group">
							<label>Gebruikersnaam:</label>
							<input class="form-control" type="text" name="username" required
							value="<?php if (isset($_COOKIE['username'])) {
								echo $_COOKIE['username'];} ?>">
						</div>
						<div class="form-group">
							<label>Wachtwoord:</label>
							<input class="form-control" type="password" name="password" required>
						</div>
						<div class="form-group">
							<input type="checkbox" name="cookieStatus" <?php if (isset($_COOKIE['username'])) {
								echo 'checked';} ?>>
							<label>Gebruikersnaam onthouden</label>
						</div>
						<div class="form-group">
							<input class="btn btn-primary" type="submit" name="loginSubmit" value="Verzenden">
						</div>
						<?php
                        if (isset($_GET['login'])) {
                            ?>
							<p class="loginFailed">Gebruikersnaam/Wachtwoord onjuist</p>
						<?php
                        }
                         ?>
					</form>
				</div>
				<div class="tab-pane" id="register">
					<form action="PHP/scripts/authentication.php" method="post"
					onsubmit="return checkRegistForm()" id="registForm" enctype="multipart/form-data">
					<div class="form-group">
						<label>Gebruikersnaam:</label>
						<input id="registUsr" class="form-control" type="text" name="username" onblur="checkUsername(this.value)">
						<p id="usernameExists">Gebruikersnaam bestaat al</p>
						<p id="usernameEmpty">Gebruikersnaam is leeg</p>
					</div>
					<div class="form-group">
						<label>Wachtwoord:</label>
						<input id="registPassword" class="form-control" type="password" name="password" required>
					</div>
					<div class="form-group">
						<label>Wachtwoord Herhalen:</label>
						<input id="regist2ndpassword" class="form-control" type="password" name="2ndpassword" required>
					</div>
					<div class="form-group">
						<div class="input-group">
						<label class="btn btn-primary btn-file">
						Browse...<input style="display:none" class="form-control" type="file" name="imageFile">
					</label>
					<input type="text" class="form-control fileUploadPreview" readonly>
					</div>
				</div>
					<div class="form-group">
						<input class="btn btn-primary" type="submit" name="registerSubmit" value="Verzenden">
					</div>
				</form>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>

</div>
</div>
<div id="berichtMaken" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Bericht Maken</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<form action="" method="post" onsubmit="return placePost(<?= $_SESSION['ID'] ?>)">
					<div class="form-group">
						<label>Bericht:</label>
						<textarea class="form-control" cols="50" rows="4" name="make_text"></textarea>
					</div>
					<div class="form-group">
						<input class="btn btn-primary" type="submit" name="submit" value="Verzenden">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div id="uitloggen" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Uitloggen</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<p>Weet u zeker dat u wilt uitloggen?</p>
				<a href="PHP/scripts/logout.php">Jazeker!</a>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div id="berichtVerwijderen" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Bericht Verwijderen</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body" id="verwijderModal">
				<form onsubmit="return delPost()">
					<input type="submit" name="submit" class="btn btn-primary">
					<input type="hidden" name="verwijderID" value="">
				</form>
				<p id="verwijderTekst"></p>
				<a id="verwijderLink"></a>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php
if (isset($_GET['login'])) {
    echo "<script>$('#loginRegist').modal();</script>";
}
?>
<script src="js/functions.js"></script>
<script>
$(function() {

  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if(input.length) {
              input.val(log);
          } else {
              if(log) alert(log);
          }

      });
  });

});

function placePosts(json) {
	var length = json.length;
	var i;
	<?php if (isset($_SESSION['user'])) {
     print 'var sessionUser = "' . $_SESSION['user'] . '";';
 } else {
     print 'var sessionUser = "";';
 }
    ?>
	document.getElementById('msgContainer').innerHTML = '';
	for (i = 0; i < length; i++) {
		var col12 = document.createElement("div");
		col12.className = "col-12";
		var cardborder = document.createElement("div");
		cardborder.className = "card border-primary";
		var header = document.createElement("div");
		header.className = "card-header";
		var lower = json[i]['username'];
		var text = document.createTextNode(lower.charAt(0).toUpperCase() + lower.substr(1));
		header.appendChild(text);
		var img = document.createElement('img');
		img.src = json[i]['image'];
		img.className = 'header-image';
		header.appendChild(img);
		var body = document.createElement("div");
		body.className = "card-body posttext";
		var text = document.createTextNode(json[i]['post_text']);
		body.appendChild(text);
		var footer = document.createElement("div");
		footer.className = "card-footer";
		var text = document.createTextNode(json[i]['date']);
		footer.appendChild(text);
		col12.appendChild(cardborder);
		cardborder.appendChild(header);
		cardborder.appendChild(body);
		cardborder.appendChild(footer);
		if (json[i]['username'] === sessionUser) {
			var footer2 = document.createElement("div");
			footer2.className = 'card-footer';
			var btngroup = document.createElement('div');
			btngroup.className = 'btn-group';
			var aanpassen = document.createElement('a');
			aanpassen.className = 'btn btn-primary';
			var text = document.createTextNode('Aanpassen');
			aanpassen.href = '';
			var att1 = document.createAttribute('data-toggle');
			att1.value = 'modal';
			var att2 = document.createAttribute('onclick');
			att2.value = 'aanpasBericht(this, ' + json[i]['post_ID'] + ')';
			aanpassen.setAttributeNode(att1);
			aanpassen.setAttributeNode(att2);
			aanpassen.appendChild(text);

			var verwijderen = document.createElement('a');
			verwijderen.className = 'btn btn-secondary';
			var verwijderTarget = '#berichtVerwijderen';
			var att1 = document.createAttribute('data-toggle');
			att1.value = 'modal';
			var att2 = document.createAttribute('onclick');
			att2.value = 'delBericht(' + json[i]['post_ID'] + ')';
			verwijderen.setAttributeNode(att1);
			verwijderen.setAttributeNode(att2);
			verwijderen.href = verwijderTarget;
			var text = document.createTextNode('Verwijderen');
			verwijderen.appendChild(text);

			btngroup.appendChild(aanpassen);
			btngroup.appendChild(verwijderen);
			footer2.appendChild(btngroup);
			cardborder.appendChild(footer2);
		}
		document.getElementById('msgContainer').appendChild(col12);
	}
}
</script>
</body>
</html>
