<?php
include 'connection.php';
if (isset($_POST['submit'])) {
    addPost($_SESSION['ID'], $_POST['post_text']);
}
if (isset($_POST['aanpasSubmit'])) {
    editPost($_POST['post_text'], $_POST['post_id']);
}
if (isset($_GET['target'])) {
    switch ($_GET['target']) {
        case 'place':
            addPost($_GET['id'], $_GET['text']);
            break;
        case 'del':
            delPost($_GET['id']);
            break;
        case 'edit':
            editPost($_GET['text'], $_GET['id']);
            break;
    }
}
function addPost($user_ID, $post_text)
{
    global $conn;
    $stmt = $conn->prepare('INSERT INTO posts (user_ID, post_text) VALUES (?, ?)');
    $stmt->bind_param('ss', $user_ID, $post_text);
    $stmt->execute();
    $stmt->close();
}

function editPost($text, $id)
{
    global $conn;
    $stmt = $conn->prepare('UPDATE posts SET post_text = ? WHERE post_ID = ?');
    $stmt->bind_param('si', $text, $id);
    $stmt->execute();
    $stmt->close();
}

function delPost($post_ID)
{
    global $conn;
    $stmt = $conn->prepare('DELETE FROM posts WHERE post_ID = ?');
    $stmt->bind_param('i', $post_ID);
    $stmt->execute();
    $stmt->close();
}

header('Location: ../../index.php');
