<?php
include 'connection.php';
ini_set('file_uploads', 'On');
if (isset($_POST['loginSubmit'])) {
	if (isset($_POST['cookieStatus'])) {
    login($_POST['username'], $_POST['password'], $_POST['cookieStatus']);
} else {
	login($_POST['username'], $_POST['password'], '');
}
}
if (isset($_POST['registerSubmit'])) {
    register($_POST['username'], $_POST['password']);
}
if (isset($_GET['target']) && $_GET['target'] === 'checkUsr') {
    checkUsername($_GET['name']);
}

function checkUsername($username)
{
    if (strlen($username) > 0) {
        global $conn;
        $stmt = $conn->prepare('SELECT username FROM users WHERE username=?');
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $stmt->bind_result($dbUsername);
        //var_dump($stmt);
        while ($stmt->fetch()) {
            if ($username === $dbUsername) {
                echo 'true';
            }
        }
    } else {
		echo 'empty';
	}
}

function login($user, $psw, $cookieStatus)
{
    global $conn;
    $stmt = $conn->prepare('SELECT user_ID, username, password, image FROM users WHERE username=?');
    $stmt->bind_param('s', $user);
    $stmt->execute();
    $stmt->bind_result($dbUser_ID, $dbUser, $dbPassword, $dbImage);

    while ($stmt->fetch()) {
        if ($dbUser === $user && password_verify($psw, $dbPassword)) {
            $_SESSION['user'] = $dbUser;
            $_SESSION['ID'] = $dbUser_ID;
            $_SESSION['image'] = $dbImage;
            $_SESSION['sid'] = session_id();
            if ($cookieStatus === 'on') {
                setcookie('username', $dbUser, time() +(86400 * 30), '/');
            } else {
                unset($_COOKIE['username']);
                setcookie('username', '', time() - 3600, '/');
            }
            header('Location: ../../index.php');
        }
    }
    $stmt->close();
    if (isset($_SESSION['user'])) {
    } else {
        header('Location: ../../index.php?login=failed');
    }
}

function register($user, $psw)
{
    global $conn;
    $userExists = false;
    $stmt = $conn->prepare('SELECT username FROM users WHERE username=?');
    $stmt->bind_param('s', $user);
    $stmt->execute();
    $stmt->bind_result($dbUser);
    while ($stmt->fetch()) {
        if ($user === $dbUser) {
            $userExists = true;
        }
    }
    if (!$userExists) {
        if ($_FILES['imageFile']['error'] === 0) {
            $target_dir = "uploads/";
            $uploadOk = 1;
            $extHelp = $target_dir . basename($_FILES["imageFile"]["name"]);
            $imageFileType = strtolower(pathinfo($extHelp, PATHINFO_EXTENSION));
            $ext = substr($_FILES['imageFile']['name'], -4);
            $md5dFile = md5(str_replace($ext, '', $_FILES['imageFile']['name']));
            $target_file = $target_dir . $md5dFile . $ext;
            $check = getimagesize($_FILES["imageFile"]["tmp_name"]);
            if ($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
            if (file_exists($target_file)) {
                $uploadOk = 0;
            }
            if ($_FILES["imageFile"]["size"] > 500000) {
                $uploadOk = 0;
            }
            if ($imageFileType !== "jpg" && $imageFileType !== "png" && $imageFileType !== "jpeg"
                  && $imageFileType !== "gif") {
                $uploadOk = 0;
            }
            if ($uploadOk === 1) {
                move_uploaded_file($_FILES["imageFile"]["tmp_name"], '../../' . $target_file);
            }
        } else {
            $target_file = 'uploads/default.png';
        }
        $hashedPsw = password_hash($psw, PASSWORD_DEFAULT);
        $stmt = $conn->prepare('INSERT INTO users (username, password, image) VALUES (?, ?, ?)');
        $stmt->bind_param('sss', $user, $hashedPsw, $target_file);
        $stmt->execute();
        header('Location: ../../index.php');
    } else {
        //echo 'Gebruiker bestaat al';
    }
    $stmt->close();
}
