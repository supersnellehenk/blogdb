<?php
include 'connection.php';
$count = 0;
$stmt = $conn->prepare('SELECT * FROM `posts` INNER JOIN `users` ON `posts`.`user_ID` = `users`.`user_ID` ORDER BY date');
$stmt->execute();
$stmt->bind_result($db_post_ID, $db_user_ID, $db_post_text, $db_date, $db_user_ID, $db_username, $db_psw, $db_image);
while ($stmt->fetch()) {
    $jsonString[$count]['user_ID'] = $db_user_ID;
    $jsonString[$count]['post_text'] = $db_post_text;
    $jsonString[$count]['date'] = $db_date;
    $jsonString[$count]['post_ID'] = $db_post_ID;
    $jsonString[$count]['username'] = $db_username;
    $jsonString[$count]['image'] = $db_image;
    $count++;
}
echo json_encode($jsonString);
