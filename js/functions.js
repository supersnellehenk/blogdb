function getPosts() {
	$.post('PHP/scripts/getPosts.php', function(response) {
		placePosts(JSON.parse(response));
	});
}

function placePost(id) {
	var text = document.getElementsByName('make_text')[0];
	var insertText = text.value;
	console.log(text.value);
	text.value = '';
	$.post('PHP/scripts/berichtScripts.php?target=place&id=' + id + '&text=' + encodeURIComponent(insertText), function() {
		getPosts()
	});
	$('#berichtMaken').modal('hide');
	return false;
}

function delPost(id) {
	$.post('PHP/scripts/berichtScripts.php?target=del&id=' + id, function() {
		getPosts()
	});
	$('#berichtVerwijderen').modal('hide');
	return false;
}



function delBericht(id) {
	var modal = document.getElementById('verwijderModal');
	modal.innerHTML = '<form onsubmit="return delPost(' + id + ')">' +
		'<input type="submit" name="submit" value="Ja dat wil ik!" class="btn btn-danger"></form>';
}

function checkUsername(username) {
	$.post('PHP/scripts/authentication.php?target=checkUsr&name=' + username, function(response) {
		if (response === 'true') {
			document.getElementById('usernameExists').style.display = 'inline-block';
			document.getElementById('usernameEmpty').style.display = 'none';
			usernameExists = true;
		} else if (response === 'empty') {
			document.getElementById('usernameExists').style.display = 'none';
			document.getElementById('usernameEmpty').style.display = 'inline-block';
		} else {
			document.getElementById('usernameExists').style.display = 'none';
			document.getElementById('usernameEmpty').style.display = 'none';
			usernameExists = false;
		}
	})
	return usernameExists;
}

function verifyPassword(username) {
	var psw1 = document.getElementById('registPassword').value;
	var psw2 = document.getElementById('regist2ndpassword').value;

	return psw1 === psw2;
}

function checkRegistForm() {
	if (checkUsername(document.getElementById('registUsr').value) === false && verifyPassword()) {
		return true;
	}
	return false;
}

var aangepast = 0;
var aanpasOriginal = '';

function editPost(id) {
	var text = document.getElementsByName('edit_text')[0].value;
	$.post('PHP/scripts/berichtScripts.php?target=edit&id=' + id + '&text=' + encodeURIComponent(text), function() {
		aangepast = 0;
		getPosts()
	});
	return false;
}

function aanpasBericht(e, i) {
	if (aangepast === 0) {
		var div = e.parentNode.parentNode.parentNode;
		var body = div.getElementsByClassName('card-body')[0];
		var bodyInner = body.innerHTML;
		body.innerHTML = '<form method="post" onsubmit="return editPost(' + i + ')">' +
			'<textarea name="edit_text" class="form-control" rows="5">' + bodyInner + '</textarea>' +
			'<input type="submit" name="aanpasSubmit" class="btn btn-primary" value="Verzenden">' +
			'<input type="reset" class="btn btn-danger" onclick="undoEdit(this)" value="Reset">';
	}
	aangepast = 1;
	aanpasOriginal = bodyInner;
}

function undoEdit(e) {
	var div = e.parentNode.parentNode.parentNode;
	var body = div.getElementsByClassName('card-body')[0];
	var text = document.getElementsByName('edit_text')[0].value;
	body.innerHTML = aanpasOriginal;
	aangepast = 0;
}
